#pragma once

#if defined( HMON_CLAW_BUILD )
#  include "claw/base/Errors.hpp"

#  define HMON_ASSERT( x ) { CLAW_ASSERT( x ); }
#  define HMON_LOG( x ) { CLAW_MSG( x );}
#  define HMON_MSG_ASSERT( x, y ) { CLAW_MSG_ASSERT( x, y ); }

#else
#  if defined( _DEBUG )
#    if defined( HMON_LIB_WIN32 ) || defined( HMON_LIB_MACOS ) || defined( HMON_LIB_IOS ) || defined( HMON_LIB_UNIX )
#      include <sstream>
#      include <cassert>
#      include <string>

#      define HMON_ASSERT(x) { assert( x ); }
#      define HMON_LOG(x) { std::ostringstream __buf; __buf << x; printf( "[HMON_LOG]: %s\n", __buf.str().c_str() ); }
#      define HMON_MSG_ASSERT(x,y) { assert( x && y ); }
#    elif defined HMON_LIB_ANDROID
#      include <android/log.h>
#      include <sstream>
#      include <string>

#      define HMON_ASSERT(x) { if( !( x ) ) __android_log_assert(#x, "HMON", #x); }
#      define HMON_LOG(x) { std::ostringstream __buf; __buf << x; __android_log_print(ANDROID_LOG_INFO,"HMON","%s\n",__buf.str().c_str()); }
#      define HMON_MSG_ASSERT(x,y) { if( !( x ) ) { std::ostringstream __buf; __buf << y; __android_log_assert( #x, "HMON", "%s\n", __buf.str().c_str() ); } }
#    else
#      error "Unsupported platform!"
#    endif
#  else
#    define HMON_ASSERT(x) {}
#    define HMON_MSG_ASSERT(x,y) {}
#    define HMON_LOG(x) {}
#  endif
#endif

#if defined( HMON_LIB_IOS ) || defined( CLAW_IPHONE )
#  define HMON_IOS
#endif
