#pragma once

#include <atomic>
#include <condition_variable>
#include <mutex>
#include <queue>
#include <sys/socket.h>
#include <thread>

#include "HMProto.hpp"

namespace HuuugeMonitoring
{
    class Monitoring
    {
    public:
        ~Monitoring();

        static Monitoring& GetInstance();
        static bool Init( const char* host, uint16_t port );

        void Enqueue( const Message& msg );
        void Enqueue( Message&& msg );
        void Shutdown();

    private:
        Monitoring();

        void DoSend( const struct msghdr* msgHdr );
        void Send( struct msghdr* msgHdr, const RawMessage& msg );
        void Send( struct msghdr* msgHdr, const void* msg );
//         void Send( struct msghdr* msgHdr, const JSONNode& msg );
        void Send( struct msghdr* msgHdr, const LoginResponseMessage& msg );

        void Worker();

        bool m_shutdown = false;
        int m_socket = -1;

        std::thread m_thread;
        std::mutex m_queueLock;
        std::condition_variable m_queueCv;
        std::queue <Message> m_queue;
    };
}
