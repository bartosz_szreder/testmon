#include <arpa/inet.h>
#include <assert.h>
#include <endian.h>
#include <netdb.h>
#include <netinet/in.h>
#include <inttypes.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "HMErrors.hpp"
#include "HMMonitoring.hpp"

namespace
{
    inline void Write( uint8_t*& dst, uint8_t src )
    {
        *dst++ = src;
    }

    inline void Write( uint8_t*& dst, uint16_t src )
    {
        const auto tmp = htons( src );
        memcpy( dst, &tmp, sizeof( tmp ) );
        dst += sizeof( tmp );
    }

    inline void Write( uint8_t*& dst, uint32_t src )
    {
        const auto tmp = htonl( src );
        memcpy( dst, &tmp, sizeof( tmp ) );
        dst += sizeof( tmp );
    }

    inline void Write( uint8_t*& dst, uint64_t src )
    {
#if defined( CLAW_ANDROID ) || defined( CLAW_UNIX )
        const auto tmp = htobe64( src );
#else
        const auto tmp = htonll( src );
#endif
        memcpy( dst, &tmp, sizeof( tmp ) );
        dst += sizeof( tmp );
    }

    std::unique_ptr<HuuugeMonitoring::Monitoring> s_monitoring;
}

namespace HuuugeMonitoring
{
    Monitoring& Monitoring::GetInstance()
    {
        return *s_monitoring;
    }

    void Monitoring::Enqueue( const Message& msg )
    {
        if( !m_thread.joinable() ) return;
        std::lock_guard<std::mutex> lock( m_queueLock );
        m_queue.push( msg );
        m_queueCv.notify_one();
    }

    void Monitoring::Enqueue( Message&& msg )
    {
        if( !m_thread.joinable() ) return;
        std::lock_guard<std::mutex> lock( m_queueLock );
        m_queue.emplace( std::move( msg ) );
        m_queueCv.notify_one();
    }

    void Monitoring::Shutdown()
    {
        std::lock_guard<std::mutex> lock( m_queueLock );
        m_shutdown = true;
        m_queueCv.notify_one();
    }

    Monitoring::Monitoring()
    {
    }

    Monitoring::~Monitoring()
    {
        std::unique_lock<std::mutex> lock( m_queueLock );
        if( !m_shutdown )
        {
            m_shutdown = true;
            m_queueCv.notify_one();
        }
        lock.unlock();

        if( m_thread.joinable() )
        {
            m_thread.join();
        }

        if( m_socket != -1 )
        {
            close( m_socket );
        }

        //TODO free queue objects
    }

    bool Monitoring::Init( const char* host, uint16_t port )
    {
        assert( !s_monitoring );
        s_monitoring.reset( new Monitoring{} );

        struct addrinfo hints;
        memset( &hints, 0, sizeof( hints ) );
        hints.ai_flags = AI_NUMERICSERV;
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_DGRAM;

        struct addrinfo* addrResult;
        char service[10];
        sprintf( service, "%" PRIu16, port );

        if( getaddrinfo( host, service, &hints, &addrResult ) != 0 )
        {
            HMON_LOG( "getaddrinfo() failed: " << strerror( errno ) );
            return false;
        }

        int sockFd = socket( addrResult->ai_family, addrResult->ai_socktype, addrResult->ai_protocol );
        if( sockFd == -1 )
        {
            HMON_LOG( "socket() failed: " << strerror( errno ) );
            freeaddrinfo( addrResult );
            return false;
        }

        if( connect( sockFd, addrResult->ai_addr, addrResult->ai_addrlen ) != 0 )
        {
            HMON_LOG( "connect() failed: " << strerror( errno ) );
            freeaddrinfo( addrResult );
            close( sockFd );
            return false;
        }

        freeaddrinfo( addrResult );
        s_monitoring->m_socket = sockFd;
        s_monitoring->m_thread = std::thread( []{ s_monitoring->Worker(); } );
        return true;
    }

    void Monitoring::DoSend( const struct msghdr* msgHdr )
    {
        if( sendmsg( m_socket, msgHdr, 0 ) == -1 )
        {
            HMON_LOG( "sendmsg() failed: " << strerror( errno ) );
        }
    }

    void Monitoring::Send( struct msghdr* msgHdr, const RawMessage& msg )
    {
        msgHdr->msg_iov[1] = { const_cast<void*>( static_cast<const void*>( &msg.data ) ), msg.size };
        DoSend( msgHdr );
    }

    void Monitoring::Send( struct msghdr* msgHdr, const void* msg )
    {
    }

//     void Monitoring::Send( struct msghdr* msgHdr, const JSONNode& msg )
//     {
//         auto strMsg = msg.write_formatted();
//         msgHdr->msg_iov[1] = { strMsg.data(), strMsg.size() };
//         DoSend( msgHdr );
//     }

    void Monitoring::Send( struct msghdr* msgHdr, const LoginResponseMessage& msg )
    {
        uint8_t buff[sizeof( msg.timestamp ) + sizeof( msg.status )];
        uint8_t* ptr = buff;

        Write( ptr, msg.timestamp );
        Write( ptr, msg.status );

        msgHdr->msg_iov[1] = { buff, sizeof( buff ) };
        DoSend( msgHdr );
    }

    void Monitoring::Worker()
    {
        struct msghdr msgHdr;
        struct iovec msgParts[2];
        uint16_t msgType;
        msgParts[0] = { &msgType, sizeof( msgType ) };

        memset( &msgHdr, 0, sizeof( msgHdr ) );
        msgHdr.msg_iov = msgParts;
        msgHdr.msg_iovlen = 2;

        while( true )
        {
            std::unique_lock<std::mutex> lock( m_queueLock );
            m_queueCv.wait( lock, [this] { return m_shutdown || !m_queue.empty(); } );

            if( m_shutdown ) break;

            auto msgVariant = m_queue.front();
            m_queue.pop();
            lock.unlock();

            msgType = htons( static_cast<uint16_t>( msgVariant.index() ) );
#if !defined( CLAW_IPHONE )
            std::visit( [this, &msgHdr](auto&& msg){ Send( &msgHdr, msg ); }, msgVariant );
            if( MessageType( msgVariant.index() ) == MessageType::Raw )
            {
                delete[] std::get<RawMessage>( msgVariant ).data;
            }
#else
            #define SUBCASE(v) case v: Send( &msgHdr, *std::get_if<v>( &msgVariant ) ); break
            switch( msgVariant.index() )
            {
                SUBCASE(0);
                SUBCASE(1);
                SUBCASE(2);
            }
            #undef SUBCASE

            if( MessageType( msgVariant.index() ) == MessageType::Raw )
            {
                delete[] std::get_if<0>( &msgVariant )->data;
            }
#endif
        }
    }
}
