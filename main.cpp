#include "HMErrors.hpp"
#include "HMMonitoring.hpp"
#include <unistd.h>

int main( int argc, char** argv )
{
    if( argc < 4 )
    {
        fprintf( stderr, "Usage: %s <host> <port> <status>\nExample: %s huuugecasino.com 1234 1\n", argv[0], argv[0] );
        return 1;
    }

    if( !HuuugeMonitoring::Monitoring::Init( argv[1], atoi( argv[2] ) ) )
    {
        HMON_LOG( "HuuugeMonitoring initialization failed" );
        return 1;
    }

    HMON_LOG( "Sending test message..." );
    auto& hmon = HuuugeMonitoring::Monitoring::GetInstance();
    hmon.Enqueue( HuuugeMonitoring::LoginResponseMessage{ static_cast<uint64_t>( time( nullptr ) ), static_cast<uint8_t>( atoi( argv[3] ) ) } );
    sleep( 1 );

    return 0;
}
