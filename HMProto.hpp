#pragma once

#include <stdint.h>
#include <variant>

// #include "libjson.h"

namespace HuuugeMonitoring
{
    //keep this in the same order as in the Message variant definition below
    enum class MessageType : uint16_t
    {
        Raw = 0,
        JSON = 1,
        LoginResponse = 2,
    };

    //ownership of the data pointer is passed to the monitoring code
    struct RawMessage
    {
        uint8_t* data;
        size_t size;
    };

    struct LoginResponseMessage
    {
        uint64_t timestamp;
        uint8_t status;
    };

    using Message = std::variant <RawMessage, void*, LoginResponseMessage>;
}
